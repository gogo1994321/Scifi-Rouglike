﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

enum RoomDirections
{
    Bal,
    Padlo,
    Jobb,
    Plafon
}

enum DirectionType
{
    Fal,
    Ajto
}

class Room {
    public GameObject roomP;
    public Dictionary<RoomDirections,DirectionType> directions = new Dictionary<RoomDirections, DirectionType>();
}

public class MazeGenerator : MonoBehaviour {

    public string roomDefinitionListName;

    public int gridSize;

    public Vector2 roomDimensions;

    List<Room> roomDefList = new List<Room>();

    Room[,] grid;

    public List<GameObject> importantToSpawn;   //Things we want to spawn unconditionally after finishing with the rooms

    public List<GameObject> enemyTypes;

    public Vector2 minMaxAmountOfEnemies;
	
	void Start ()
    {
        if (!string.IsNullOrEmpty(roomDefinitionListName))
        {
            string file = System.IO.File.ReadAllText("Assets/Resources/"+roomDefinitionListName+".csv");
            string[] lines = file.Split("\n"[0]);
            foreach (string line in lines)
            {
               //Debug.Log(line);
                string[] lineData = (line.Trim()).Split(","[0]);
                Room room = new Room();
                room.roomP = Resources.Load("RoomPrefabs/" + lineData[0]) as GameObject;
                for (int i = 1; i < lineData.Length; i++)
                {
                    if (!string.IsNullOrEmpty(lineData[i]))
                    {
                        room.directions.Add((RoomDirections)i - 1, (DirectionType)System.Enum.Parse(typeof(DirectionType),lineData[i]));
                    }
                }
                Debug.Log("Room with name: "+room.roomP.name+" and: "+ToDebugString(room.directions));
                roomDefList.Add(room);
            }
        }

        StartGenerating();
        //TestAndFixContinuity();

        List<Room> usedRooms = new List<Room>();
        foreach (GameObject item in importantToSpawn)
        {
            SpawnImportantThing(ref usedRooms, item);
        }

        SpawnEnemies(ref usedRooms);

        FindObjectOfType<GameManager>().AfterGeneration();
	}

    void StartGenerating()
    {
        grid = new Room[gridSize, gridSize];
        List<Vector2> nextRoomPositions = new List<Vector2>();
        nextRoomPositions.Add(new Vector2(gridSize/2, gridSize / 2));
        while(nextRoomPositions.Count > 0)
        {
            Vector2 position = nextRoomPositions[Random.Range(0, nextRoomPositions.Count)];
            Dictionary<RoomDirections, DirectionType> constraints = calculateGeneralConstraints((int)position.x, (int)position.y);
            spawnRoomWithConstraints(ref constraints, (int)position.x, (int)position.y, ref nextRoomPositions);
            nextRoomPositions.Remove(position);
        }          
    }

    Dictionary<RoomDirections, DirectionType> calculateGeneralConstraints(int w, int h)
    {
        Dictionary<RoomDirections, DirectionType> constraints = new Dictionary<RoomDirections, DirectionType>();
        //Out of bounds proofing
        if (w == 0)
        {
            constraints.Add(RoomDirections.Bal, DirectionType.Fal);
        }
        if (w == gridSize - 1)
        {
            constraints.Add(RoomDirections.Jobb, DirectionType.Fal);
        }
        if (h == 0)
        {
            constraints.Add(RoomDirections.Padlo, DirectionType.Fal);
        }
        if (h == gridSize - 1)
        {
            constraints.Add(RoomDirections.Plafon, DirectionType.Fal);
        }

        //Test neighbors
        if (w - 1 >= 0 && grid[w - 1, h] != null && !constraints.ContainsKey(RoomDirections.Bal))
        {
            constraints.Add(RoomDirections.Bal, grid[w - 1, h].directions[RoomDirections.Jobb]);
        }
        if (w + 1 < gridSize && grid[w + 1, h] != null && !constraints.ContainsKey(RoomDirections.Jobb))
        {
            constraints.Add(RoomDirections.Jobb, grid[w + 1, h].directions[RoomDirections.Bal]);
        }
        if (h - 1 >= 0 && grid[w, h - 1] != null && !constraints.ContainsKey(RoomDirections.Padlo))
        {
            constraints.Add(RoomDirections.Padlo, grid[w, h - 1].directions[RoomDirections.Plafon]);
        }
        if (h + 1 < gridSize && grid[w, h + 1] != null && !constraints.ContainsKey(RoomDirections.Plafon))
        {
            constraints.Add(RoomDirections.Plafon, grid[w, h + 1].directions[RoomDirections.Padlo]);
        }
        return constraints;
    }

    void spawnRoomWithConstraints(ref Dictionary<RoomDirections, DirectionType>  constraints, int w, int h, ref List<Vector2> nextRoomPositions)
    {
        if (grid[w, h] != null)
            return;

       Room selectedRoom = null;
       List <Room> possibleRooms = new List<Room>();
        foreach (Room possibleRoom in roomDefList)
        {
            bool matching = true;
            foreach (KeyValuePair<RoomDirections, DirectionType> constraint in constraints)
            {
                if (possibleRoom.directions[constraint.Key] != constraint.Value)    //If we find a constraint that doesn't match up with current room, then drop everything and move on
                {
                    matching = false;
                    break;
                }
            }

            if (matching)
                possibleRooms.Add(possibleRoom);
        }
        //Debug.Log("W: "+w+" H: "+h+" -> "+ToDebugString(constraints));
        if (possibleRooms.Count > 0)
            selectedRoom = possibleRooms[Random.Range(0, possibleRooms.Count)];

        if (selectedRoom != null)
        {
            GameObject spawnedRoom = Instantiate(selectedRoom.roomP, new Vector3(w * roomDimensions.x, h * roomDimensions.y, 0), Quaternion.identity);
            selectedRoom.roomP = spawnedRoom;
            grid[w, h] = selectedRoom;
            foreach (KeyValuePair<RoomDirections,DirectionType> kvP in selectedRoom.directions)
            {
                if (kvP.Value == DirectionType.Ajto)
                {
                    switch (kvP.Key)
                    {
                        case RoomDirections.Bal:
                            if (grid[w - 1, h] == null)
                            {
                                nextRoomPositions.Add(new Vector2(w - 1, h));
                            }
                            break;
                        case RoomDirections.Jobb:
                            if (grid[w + 1, h] == null)
                            {
                                nextRoomPositions.Add(new Vector2(w + 1, h));
                            }
                            break;
                        case RoomDirections.Plafon:
                            if (grid[w, h+1] == null)
                            {
                                nextRoomPositions.Add(new Vector2(w, h+1));
                            }
                            break;
                        case RoomDirections.Padlo:
                            if (grid[w, h-1] == null)
                            {
                                nextRoomPositions.Add(new Vector2(w, h-1));
                            }
                            break;
                    }                        
                }
            }
        }
        else
        {
            Debug.LogError("Found a constraint setup that isn't possible to satisfy!: " + ToDebugString(constraints));
        }
    }

    void SpawnImportantThing(ref List<Room> usedRooms, GameObject toSpawn)
    {
        if (toSpawn)
        {
            Room room = null;
            bool foundOther = false;
            while (!foundOther)
            {
                room = grid[Random.Range(0, gridSize), Random.Range(0, gridSize)];
                if (!usedRooms.Contains(room) && room != null)
                    foundOther = true;
            }
            usedRooms.Add(room);
            if (room.roomP.GetComponent<RoomInfo>() && room.roomP.GetComponent<RoomInfo>().spawnLocations.Count > 0)
            {
                List<GameObject> spawnPoints = room.roomP.GetComponent<RoomInfo>().spawnLocations;
                Instantiate(toSpawn, spawnPoints[Random.Range(0,spawnPoints.Count)].transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(toSpawn, room.roomP.transform.position, Quaternion.identity);
            }
        }
    }

    void SpawnEnemies(ref List<Room> usedRooms)
    {
        int amount = Random.Range((int)minMaxAmountOfEnemies.x, (int) minMaxAmountOfEnemies.y);
        while (amount > 0)
        {
            Room room = null;
            bool foundOther = false;
            while (!foundOther)
            {
                room = grid[Random.Range(0, gridSize), Random.Range(0, gridSize)];
                if (!usedRooms.Contains(room) && room != null)
                    foundOther = true;
            }
            List<GameObject> spawnPoints = room.roomP.GetComponent<RoomInfo>().spawnLocations;
            Instantiate(enemyTypes[Random.Range(0,enemyTypes.Count)], spawnPoints[Random.Range(0,spawnPoints.Count)].transform.position, Quaternion.identity);
            amount--;
        }
    }

    /*void TestAndFixContinuity() //Agent that goes through the maze and tries to fix disconnected areas
    {
        bool allAreasConnected = false;
        int maxTries = 1000;
        while (!allAreasConnected && maxTries > 0)
        {
            List<Vector2> visitedRooms = new List<Vector2>(); //Keep track of all the rooms we visited once
            Vector2 currentCoords = new Vector2(0, 0);
            visitedRooms.Add(currentCoords);
            List<Vector2> pathTaken = new List<Vector2>();    //Keep track of the current path (room coordinates) so we can backtrack once reached a dead end
            pathTaken.Add(currentCoords);


            while (pathTaken.Count > 0) //Go through all the currently avaiable paths and mark them as visited
            {
                List<RoomDirections> possibleDirectionsDoor = new List<RoomDirections>(); //grid[(int)currentCoords.x, (int)currentCoords.y].directions.Where(x => x.Value == DirectionType.Ajto).Select(x => x.Key) as List<RoomDirections>;  //Select every direction that has a door
                Debug.Log(currentCoords);
                if (grid[(int)currentCoords.x, (int)currentCoords.y] != null)
                {
                    foreach (KeyValuePair<RoomDirections, DirectionType> pair in grid[(int)currentCoords.x, (int)currentCoords.y].directions)
                    {
                        if (pair.Value == DirectionType.Ajto)
                            possibleDirectionsDoor.Add(pair.Key);
                    }
                }

                var randomDir = possibleDirectionsDoor[Random.Range(0, possibleDirectionsDoor.Count)];

                switch (randomDir)
                {
                    case RoomDirections.Bal:
                        if (!visitedRooms.Contains(new Vector2(currentCoords.x - 1, currentCoords.y)))
                        {
                            currentCoords = new Vector2(currentCoords.x - 1, currentCoords.y);
                            visitedRooms.Add(currentCoords);
                            pathTaken.Add(currentCoords);
                        }
                        else
                        {
                            pathTaken.Remove(currentCoords);
                            if(pathTaken.Count > 0)
                                currentCoords = pathTaken.Last();
                        }
                        break;
                    case RoomDirections.Jobb:
                        if (!visitedRooms.Contains(new Vector2(currentCoords.x + 1, currentCoords.y)))
                        {
                            currentCoords = new Vector2(currentCoords.x + 1, currentCoords.y);
                            visitedRooms.Add(currentCoords);
                            pathTaken.Add(currentCoords);
                        }
                        else
                        {
                            pathTaken.Remove(currentCoords);
                            if (pathTaken.Count > 0)
                                currentCoords = pathTaken.Last();
                        }
                        break;
                    case RoomDirections.Padlo:
                        if (!visitedRooms.Contains(new Vector2(currentCoords.x, currentCoords.y - 1)))
                        {
                            currentCoords = new Vector2(currentCoords.x, currentCoords.y - 1);
                            visitedRooms.Add(currentCoords);
                            pathTaken.Add(currentCoords);
                        }
                        else
                        {
                            pathTaken.Remove(currentCoords);
                            if (pathTaken.Count > 0)
                                currentCoords = pathTaken.Last();
                        }
                        break;
                    case RoomDirections.Plafon:
                        if (!visitedRooms.Contains(new Vector2(currentCoords.x, currentCoords.y + 1)))
                        {
                            currentCoords = new Vector2(currentCoords.x, currentCoords.y + 1);
                            visitedRooms.Add(currentCoords);
                            pathTaken.Add(currentCoords);
                        }
                        else
                        {
                            pathTaken.Remove(currentCoords);
                            if (pathTaken.Count > 0)
                                currentCoords = pathTaken.Last();
                        }
                        break;

                    default:
                        pathTaken.Remove(currentCoords);
                        if (pathTaken.Count > 0)
                            currentCoords = pathTaken.Last();
                        break;
                }

                maxTries--;
            }

            List<Vector2> notVisitedRooms = new List<Vector2>();
            for (int i = 0; i < gridSize; i++)  //Find all unvisited rooms, ergo the ones that aren't connected to this part of the maze
            {
                for (int j = 0; j < gridSize; j++)
                {
                    if (!visitedRooms.Contains(new Vector2(i, j)))
                    {
                        notVisitedRooms.Add(new Vector2(i, j));
                    }
                }
            }

            //Let's check if we found any rooms that weren't visited, if not, then we are finished
            if (notVisitedRooms.Count <= 0)
            {
                allAreasConnected = true;
                continue;
            }

            Vector2 selectedNonVisitedRoom = notVisitedRooms[Random.Range(0, notVisitedRooms.Count)];
            Vector2 selectedVisitedRoom = new Vector2(0, 0);
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if ((i == 0 && j == 0) || (i == 1 && j == 1) || (i == -1 && j == -1) || (i == -1 && j == 1) || (i == 1 && j == -1))
                        continue;

                    if (visitedRooms.Contains(new Vector2(selectedNonVisitedRoom.x+i, selectedNonVisitedRoom.y + j)))   //Find a room around us that's part of the visited set, so we can connect to it
                    {
                        selectedVisitedRoom = new Vector2(selectedNonVisitedRoom.x + i, selectedNonVisitedRoom.y + j);
                    }
                }
            }
            //Get the actual room that we haven't visited and respawn it with a new door
            Room nonVisitedRoom = grid[(int)selectedNonVisitedRoom.x, (int)selectedNonVisitedRoom.y];
            if (selectedVisitedRoom.x < selectedNonVisitedRoom.x)
            {
                nonVisitedRoom.directions[RoomDirections.Bal] = DirectionType.Ajto;
            }
            if (selectedVisitedRoom.x > selectedNonVisitedRoom.x)
            {
                nonVisitedRoom.directions[RoomDirections.Jobb] = DirectionType.Ajto;
            }
            if (selectedVisitedRoom.y < selectedNonVisitedRoom.y)
            {
                nonVisitedRoom.directions[RoomDirections.Padlo] = DirectionType.Ajto;
            }
            if (selectedVisitedRoom.y > selectedNonVisitedRoom.y)
            {
                nonVisitedRoom.directions[RoomDirections.Plafon] = DirectionType.Ajto;
            }
            Destroy(nonVisitedRoom.roomP);
            spawnRoomWithConstraints(ref nonVisitedRoom.directions, (int)selectedNonVisitedRoom.x, (int)selectedNonVisitedRoom.y);

            //Select the visited room, and respawn it now that it has a new neighbor
            Room visitedRoom = grid[(int)selectedVisitedRoom.x, (int)selectedVisitedRoom.y];
            Destroy(visitedRoom.roomP);
            Dictionary<RoomDirections, DirectionType> constraints = calculateGeneralConstraints((int)selectedVisitedRoom.x, (int)selectedVisitedRoom.y);
            spawnRoomWithConstraints(ref constraints, (int)selectedVisitedRoom.x, (int)selectedVisitedRoom.y);

            Debug.Log("Connecting: "+selectedNonVisitedRoom+"  <-> "+selectedVisitedRoom);

            maxTries--;
        }
        if (maxTries == 0)
            Debug.Log("Timeout");
    }*/

    public string ToDebugString<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
    }
}
