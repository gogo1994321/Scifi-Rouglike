﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public GameObject bulletPref;

    public Vector3 bulletSpawnDisplacement;

    public float shootCoolDown;

    bool canShoot = true;

    public void Shoot()
    {
        if (canShoot)
        {
            GameObject b = Instantiate(bulletPref, transform.parent.position + (bulletSpawnDisplacement * transform.parent.localScale.x), Quaternion.identity) as GameObject;
            b.GetComponent<Bullet>().dir = -transform.parent.localScale.x;
            canShoot = false;
            StartCoroutine(ShootCooldown(shootCoolDown));
        }
    }

    IEnumerator ShootCooldown(float timer)
    {
        yield return new WaitForSeconds(timer);
        canShoot = true;
    }
}
