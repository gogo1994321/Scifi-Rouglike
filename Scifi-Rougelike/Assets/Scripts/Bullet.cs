﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [HideInInspector]
    public float dir;
    public float speed;
    public int damage;
    public AudioClip shootSound;

    float timer = 5f;

    private void Start()
    {
        GetComponent<AudioSource>().PlayOneShot(shootSound);
    }

    private void Update()
    {
        transform.Translate(new Vector3(speed * dir, 0));
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            if (collision.gameObject.GetComponent<IReceiveDamage>() != null)
            {
                collision.gameObject.GetComponent<IReceiveDamage>().ReceiveDamage(damage);
            }
            Destroy(gameObject);
        }
    }
}
