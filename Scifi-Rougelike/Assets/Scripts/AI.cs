﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour, IReceiveDamage {

    enum MovementType {
        Walk,
        Fly
    }

    enum CurrentState {
        Idle,
        Chasing,
        Fleeing,
        Waiting
    }

    enum AttackType {
        Melee,
        Range
    }

    public float playerNoticeRange;
    [SerializeField]
    MovementType movementType;
    public float speed = 6f;
    public int damageToDo;
    public float waitAfterAttackTimer;
    [SerializeField]
    AttackType attackType;
    public float attackDistance = 0.1f;
    public int maxHealth;
    public Animator animator;
    public AudioClip deathSound;
    public AudioClip noticeSound;

    private Player movementScript;
    Vector2 dirInput = new Vector2(1, 0);
    GameObject actualPlayer;
    CurrentState currentState;
    float distanceToPlayer;
    int currentHealth;
    bool canAttack = true;

    private void Start()
    {
        if (GetComponent<Player>()) {
            movementScript = GetComponent<Player>();
            movementScript.moveSpeed = speed;
        }
        currentHealth = maxHealth;
    }

    private void Update()
    {
        if(!actualPlayer)
            actualPlayer = GameObject.FindGameObjectWithTag("Player");

        if (currentHealth <= 0)
            return;

        animator.SetBool("Moving", (currentState == CurrentState.Chasing || currentState == CurrentState.Fleeing));

        distanceToPlayer = (actualPlayer.transform.position - transform.position).magnitude;
        //Debug.Log("Distance to P: "+distanceToPlayer);
        if (movementType == MovementType.Walk && movementScript && currentState == CurrentState.Idle)
        {
            movementScript.SetDirectionalInput(dirInput);
            if ((GetComponent<Controller2D>().collisions.right && dirInput.x != -1) || (GetComponent<Controller2D>().collisions.left && dirInput.x != 1))
            {
                dirInput.x *= -1;
                //Debug.Log("right: "+ GetComponent<Controller2D>().collisions.right+" left: "+ GetComponent<Controller2D>().collisions.left+" input: "+dirInput);
            }

        }

        if (currentState == CurrentState.Fleeing && distanceToPlayer >= playerNoticeRange/2)
        {
            currentState = CurrentState.Waiting;
            //Debug.Log("???");
            StartCoroutine("fleeingEnumerator");
        }

        if (distanceToPlayer <= playerNoticeRange)
        {
            //Debug.Log("Noticed player with distance: "+ (actualPlayer.transform.position - transform.position).magnitude);
            if (currentState == CurrentState.Idle)
            {
                currentState = CurrentState.Chasing;
                GetComponent<AudioSource>().PlayOneShot(noticeSound);
            }


            if (currentState != CurrentState.Waiting)
            {
                if (movementType == MovementType.Walk)
                {
                    if (actualPlayer.transform.position.x > transform.position.x)
                    {
                        dirInput = new Vector2(currentState == CurrentState.Chasing ? 1 : -1, 0);
                    }
                    else if (actualPlayer.transform.position.x < transform.position.x)
                    {
                        dirInput = new Vector2(currentState == CurrentState.Chasing ? -1 : 1, 0);
                    }
                    movementScript.SetDirectionalInput(dirInput);

                    if (actualPlayer.transform.position.y > transform.position.y+1)
                    {
                        movementScript.OnJumpInputDown();
                    }
                }
                if (movementType == MovementType.Fly)
                {
                   dirInput = (actualPlayer.transform.position - transform.position).normalized;
                    if (currentState == CurrentState.Fleeing)
                        dirInput *= -1;

                    transform.Translate(dirInput * Time.deltaTime * speed);
                }
            } 
        }
        if (distanceToPlayer <= attackDistance && canAttack)
        {
            if (attackType == AttackType.Melee)
            {
                actualPlayer.GetComponent<IReceiveDamage>().ReceiveDamage(damageToDo);
                currentState = CurrentState.Fleeing;    //Once we hit the player, then go away for a bit
            }
            else if (attackType == AttackType.Range)
            {
                transform.GetComponentInChildren<Weapon>().Shoot();
            }
            canAttack = false;
        }

        if (dirInput.x > 0)
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        }
        else if (dirInput.x < 0)
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        }
    }

    IEnumerator fleeingEnumerator()
    {
        yield return new WaitForSeconds(waitAfterAttackTimer);
        canAttack = true;
        currentState = CurrentState.Idle;
    }

    public void ReceiveDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            StartCoroutine(Dead());
        }
    }

    IEnumerator Dead()
    {
        animator.SetTrigger("Dead");
        GetComponent<AudioSource>().PlayOneShot(deathSound);
        yield return new WaitForSeconds(0.7f);
        Destroy(gameObject);
    }
}
