﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBattery : MonoBehaviour {

    public AudioClip pickupSound;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerPrefs.SetInt("battery", PlayerPrefs.GetInt("battery", 0)+1);
            collision.gameObject.GetComponent<PlayerInfo>().invincible = true;
            StartCoroutine(FindObjectOfType<GameManager>().EndOfGame(false));
            FindObjectOfType<PlayerInput>().animator.SetTrigger("Win");
            GetComponent<AudioSource>().PlayOneShot(pickupSound);
            GetComponentInChildren<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
        }
    }
}
