﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public List<GameObject> possibleEnemies;

    public int amount;

    public void Spawn()
    {
        GameObject enemyT = possibleEnemies[Random.Range(0,possibleEnemies.Count)];
        for (int i = 0; i < amount-1; i++)
        {
            Instantiate(enemyT, transform.position, Quaternion.identity);
        }
    }
}
