﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VentillationLightManager : MonoBehaviour {

    public List<GameObject> lights;

    public GameObject endPanel;

    private void Start()
    {
        if (PlayerPrefs.GetInt("battery",0) != 0)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("battery")+1; i++)
            {
                lights[i].SetActive(true);
            }
        }
        if (PlayerPrefs.GetInt("battery", 0) >= 3)
        {
            endPanel.SetActive(true);
        }
    }

    public void ClearSave() {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("Hub");
    }
}
