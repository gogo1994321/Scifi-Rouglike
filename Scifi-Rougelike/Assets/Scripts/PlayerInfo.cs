﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour, IReceiveDamage {

    public int maxHealth;
    public AudioClip deathSound;
    public float speed;

    int currentHealth;
    public bool invincible = false;
    GameManager gM;

    private void Start()
    {
        currentHealth = maxHealth;
        gM = FindObjectOfType<GameManager>();
        GetComponent<Player>().moveSpeed = speed;
    }

    private void Update()
    {
        if(gM)
            gM.ModifyHealthUI(currentHealth);
    }

    public void ReceiveDamage(int damage)
    {
        if (currentHealth > 0 && !invincible)
        {
            currentHealth -= damage;
            Debug.Log("Health: "+currentHealth);
            if (currentHealth <= 0)
            {
                GetComponent<PlayerInput>().animator.SetTrigger("Dead");
                GetComponent<AudioSource>().PlayOneShot(deathSound);
                StartCoroutine(FindObjectOfType<GameManager>().EndOfGame(true));
            }
        }
    }

    
}
