﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public CameraFollow cam;
    public Image healthUI;

    public List<Sprite> healthImages;

    public void AfterGeneration()
    {
        cam.target = GameObject.FindGameObjectWithTag("Player").GetComponent<Controller2D>();
        cam.SetupFocusArea();
    }

    public IEnumerator EndOfGame(bool playerIsDead)
    {
        if (!playerIsDead)
        {
            FindObjectOfType<PlayerInput>().animator.SetTrigger("Win");
        }

        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Hub");
    }

    public void ModifyHealthUI(int health)
    {
        if(health > 0)
            healthUI.sprite = healthImages[health-1];
    }
}
