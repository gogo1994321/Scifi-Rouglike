﻿using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
    private Player player;

    public Animator animator;

    public bool canInput = true;

    private float scaleX;

    private void Start()
    {
        player = GetComponent<Player>();
        scaleX = transform.localScale.x;
    }

    private void Update()
    {
        if (!canInput)
        {
            return;
        }

        Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        //Debug.Log(directionalInput);
        player.SetDirectionalInput(directionalInput);

        if (directionalInput.x > 0)
        {
            transform.localScale = new Vector3(-scaleX, transform.localScale.y, transform.localScale.z);
        } else if (directionalInput.x < 0)
        {
            transform.localScale = new Vector3(scaleX, transform.localScale.y, transform.localScale.z);
        }
        animator.SetBool("Moving", directionalInput.x != 0);
        animator.SetBool("InAir", GetComponent<Player>().VerticalVelocity() != 0);

        if (Input.GetButtonDown("Jump") || Input.GetAxisRaw("Vertical") > 1)
        {
            player.OnJumpInputDown();
            animator.SetTrigger("Jump");
        }

        if (Input.GetButtonUp("Jump"))
        {
            player.OnJumpInputUp();
        }

        if (Input.GetButtonDown("Attack"))
        {
            transform.GetComponentInChildren<Weapon>(true).Shoot();
            animator.SetTrigger("Attack");

        }

        if (Input.GetButtonDown("Defend"))
        { }

    }
}
